var proxy = require('express-http-proxy');
var app = require('express')();
//fix ssl localhost
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const proxyURL = 'https://localhost:3000';
const proxyPort = 4000;

app.use('/', proxy(proxyURL));
app.listen(proxyPort, function(){ console.log(proxyPort + ' listing to ' + proxyURL) });